import { useState } from "react";
import Button from "./components/Button/Button";
import ModalImage from "./components/Modal/ModalImage";
import ModalText from "./components/Modal/ModalText";
import image from "./images/image.png";

const App = () => {
  const type = "button";
  const className = "button";

  const [imageModalOpen, setImageModalOpen] = useState(false);
  const [textModalOpen, setTextModalOpen] = useState(false);

  const openImageModal = () => setImageModalOpen(true);
  const closeImageModal = () => setImageModalOpen(false);

  const openTextModal = () => setTextModalOpen(true);
  const closeTextModal = () => setTextModalOpen(false);

  return (
    <div>
      <Button onClick={openImageModal} type={type} classNames={className}>
        Open first modal
      </Button>
      <Button onClick={openTextModal} type={type} classNames={className}>
        Open second modal
      </Button>

      {imageModalOpen && (
        <ModalImage
          imageUrl={image}
          title="Product Delete!"
          text="By clicking the “Yes, Delete” button, PRODUCT NAME will be deleted."
          onClose={closeImageModal}
        />
      )}
      {textModalOpen && (
        <ModalText
          title="Add Product “NAME”"
          text="Description for you product"
          onClose={closeTextModal}
        />
      )}
    </div>
  );
};

export default App;
