import "./Button.scss";

const Button = ({ type, classNames, onClick, children }) => {
  return (
    <button className={`${classNames}`} type={type} onClick={onClick}>
      {children}
    </button>
  );
};

export default Button;
