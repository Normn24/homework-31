import "./Modal.scss";

export default function ModalClose({ onClick }) {
  return (
    <button className="modal__close" onClick={onClick}>
      &times;
    </button>
  );
}
