import "./Modal.scss";

export default function ModalFooter({
  firstText,
  secondText,
  firstClick,
  secondClick,
}) {
  return (
    <div className="modal__footer">
      {firstText && <button onClick={firstClick}>{firstText}</button>}
      {secondText && <button onClick={secondClick}>{secondText}</button>}
    </div>
  );
}
