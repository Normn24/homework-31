import ModalWrapper from "./ModalWrapper";
import Modal from "./Modal";
import ModalHeader from "./ModalHeader";
import ModalClose from "./ModalClose";
import ModalBody from "./ModalBody";
import ModalFooter from "./ModalFooter";
import "./Modal.scss";

const ModalImage = ({ imageUrl, title, text, onClose }) => {
  return (
    <ModalWrapper onClose={onClose}>
      <Modal>
        <ModalHeader>
          <ModalClose onClick={onClose} />
        </ModalHeader>
        <ModalBody>
          <img src={imageUrl} alt="Modal" />
          <h2>{title}</h2>
          <p>{text}</p>
        </ModalBody>
        <ModalFooter
          firstText="NO, CANCEL"
          firstClick={onClose}
          secondText="YES, DELETE"
          secondClick={onClose}
        />
      </Modal>
    </ModalWrapper>
  );
};

export default ModalImage;
