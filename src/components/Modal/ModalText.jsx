import ModalWrapper from "./ModalWrapper";
import Modal from "./Modal";
import ModalHeader from "./ModalHeader";
import ModalClose from "./ModalClose";
import ModalBody from "./ModalBody";
import ModalFooter from "./ModalFooter";
import "./Modal.scss";

const ModalText = ({ title, text, onClose }) => {
  return (
    <ModalWrapper onClose={onClose}>
      <Modal>
        <ModalHeader>
          <ModalClose onClick={onClose} />
        </ModalHeader>
        <ModalBody>
          <h1>{title}</h1>
          <p>{text}</p>
        </ModalBody>
        <ModalFooter firstText="ADD TO FAVORITE" firstClick={onClose} />
      </Modal>
    </ModalWrapper>
  );
};

export default ModalText;
