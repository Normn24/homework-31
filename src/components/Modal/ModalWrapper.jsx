import "./Modal.scss";

export default function ModalWrapper({ children, onClose }) {
  const handleClickOutside = (event) => {
    if (event.target === event.currentTarget) {
      onClose();
    }
  };

  return (
    <div className="modal__wrapper" onClick={handleClickOutside}>
      {children}
    </div>
  );
}
